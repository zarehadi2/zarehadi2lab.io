function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function feather_red() {
    return '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"\n' +
        '             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"\n' +
        '             class="feather feather-trending-up"style="color: rgb(214, 102, 121)">\n' +
        '            <polyline points="23 6 13.5 15.5 8.5 10.5 1 18"></polyline>\n' +
        '            <polyline points="17 6 23 6 23 12"></polyline>\n' +
        '        </svg>'
}

function feather_green() {
    return '        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"\n' +
        '             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"\n' +
        '             class="feather feather-trending-up"style="color: rgb(113, 255, 47)">\n' +
        '            <polyline points="23 6 13.5 15.5 8.5 10.5 1 18"></polyline>\n' +
        '            <polyline points="17 6 23 6 23 12"></polyline>\n' +
        '        </svg>'
}

function feather_gray() {
    return '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"\n' +
        '             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"\n' +
        '             class="feather feather-trending-up">\n' +
        '            <polyline points="23 6 13.5 15.5 8.5 10.5 1 18"></polyline>\n' +
        '            <polyline points="17 6 23 6 23 12"></polyline>\n' +
        '        </svg>'
}

let loading_row =
    '<tr>\n' +
    '   <td colspan="12">\n' +
    '       <div class="d-flex justify-content-center">\n' +
    '           <div class="spinner-border text-warning" role="status">\n' +
    '               <span class="sr-only">Loading...</span>\n' +
    '           </div>\n' +
    '       </div>\n' +
    '   </td>\n' +
    '</tr>';


function get_news() {
    let result = [];
    const settings = {
        url: "https://newsapi.org/v2/top-headlines?q=coronavirus&sortBy=publishedAt&apiKey=c7dc8ec639154578993d1ea0cad23dbd",
        type: "GET",
        async: false,
        crossDomain: true,
        crossOrigin: true,
        success: function (response) {
            // console.log(response)
            result = response.articles;
        },
    };
    $.ajax(settings);
    return result;
}

function get_today_stats_of_all_countries() {
    const settings = {
        url: "https://www.trackcorona.live/api/countries",
        type: "GET",
        async: false,
        crossDomain: true,
        crossOrigin: true,
    };
    let result = [];
    $.ajax(settings).done(function (response) {
        result = response.data;
        // console.log();
    });
    result.sort(function (a, b) {
        if (a.confirmed > b.confirmed)
            return -1
        return 1
    })

    return result;
}

function get_all_countries() {
    let res = [];
    $.ajax({
        url: "https://api.covid19api.com/countries",
        type: "GET",
        async: false,
        crossDomain: true,
        crossOrigin: true,
        success: function (result) {
            res = result;
        },
    });
    return res;
}

function perform_query(e) {
    // console.log(e.value)
    const country_selected = e.target.value;
    // console.log('country_selected ' + country_selected)
    fill_table_history(country_selected)
}

function prepare_country(element) {
    for (let item of $('#country-loading'))
        $(item).show()
    // $('#country-loading').show();
    $.ajax({
        url: "https://api.covid19api.com/countries",
        type: "GET",
        async: false,
        crossDomain: true,
        crossOrigin: true,
        success: function (result) {
            element.innerHTML = ''
            // element.innerHTML = '<option value="">Choose...</option>'
            // console.log(result)
            result.sort(function (a, b) {
                if (a.Country < b.Country) {
                    return -1;
                }
                if (a.Country > b.Country) {
                    return 1;
                }
                return 0;
            });

            for (var i in result) {
                var item = document.createElement('option');
                item.value = result[i]['Slug']
                item.setAttribute('code', result[i]['ISO2'])
                item.innerText = result[i]['Country']
                element.append(item)
            }

            for (let item of element.options) {
                // console.log(item.value)
                if (item.value === "iran") {
                    // console.log(item)
                    element.selectedIndex = item.index
                    $(element).trigger("change");
                    // $('#country').selectedIndex = $(item).index
                    break;
                }
            }
        },
        error: function (error) {
            element.innerHTML = error
        },
        complete: function () {
            for (let item of $('#country-loading'))
                $(item).hide()
            // $('#country-loading').hide();
        }
    })
}

function formatDate(date, format) {
    const map = {
        mm: date.getMonth() + 1,
        dd: date.getDate(),
        yy: date.getFullYear().toString().slice(-2),
        yyyy: date.getFullYear()
    }

    return format.replace(/mm|dd|yy|yyy/gi, matched => map[matched])
}

function show_data_in_element(element, data) {
    element.innerHTML = '';
    if (data.length === 0) {
        show_error_in_element(element, 'no data found')
    } else {
        for (const i in data) {
            if (data[i]['Confirmed'].toString() === '0')
                break;

            const tr = document.createElement('tr');
            const th = document.createElement('th');
            th.scope = 'row';
            th.innerText = (parseInt(i) + 1).toString();
            tr.append(th);

            const Country = document.createElement('td');
            Country.innerText = data[i]['Country'];
            tr.append(Country);

            const Date = document.createElement('td');
            if (!('Date' in data[i]))
                Date.innerText = '---';
            else
                Date.innerText = data[i]['Date'].split('T')[0];
            tr.append(Date);

            // if (Date.innerText === '2020-11-17')
            //     console.log(data[i])

            const cols = ['Confirmed', 'Active', 'Deaths', 'Recovered'];
            for (const j in cols) {
                // console.log(cols[j])

                let next_row = (parseInt(i) + 1).toString();

                let val_cur = parseInt(data[i][cols[j]]);
                let val_next = parseInt(data[next_row][cols[j]])
                let val = val_cur - val_next;

                if (j > 0) {
                    let confirmed_cur = parseInt(data[i]['Confirmed']);
                    let confirmed_next = parseInt(data[next_row]['Confirmed']);
                    let confirmed = confirmed_cur - confirmed_next;

                    const percent = val_cur / confirmed_cur * 100;

                    val = val_cur.toString()
                    // + ' (' + percent.toFixed(2).toString()
                    // + '%) <i style="color: ' + (val >= 0 ? 'green' : 'red') + ';">' + val.toString() + '</i>'
                } else {
                    val = val_cur.toString()
                }
                val = val.toString()
                // val = val_cur.toString()
                const item = document.createElement('td');
                // Confirmed.classList.add('text-center')
                item.innerHTML = numberWithCommas(val);
                tr.append(item);
                const growth = document.createElement('td');
                if (j === '2')
                    growth.innerHTML = feather_red() + ' ' + numberWithCommas(val_cur - val_next)
                else if (j === '3')
                    growth.innerHTML = feather_green() + ' ' + numberWithCommas(val_cur - val_next)
                else
                    growth.innerHTML = feather_gray() + ' ' + numberWithCommas(val_cur - val_next)
                tr.append(growth)
            }
            element.append(tr)
        }
    }
}

function show_error_in_element(element, error) {
    element.innerHTML = '';
    const tr = document.createElement('tr');
    const td = document.createElement('td');
    td.colSpan = 12

    const div = document.createElement('div');
    div.classList.add('d-flex')
    div.classList.add('justify-content-center');
    div.innerHTML = error;

    td.append(div)
    tr.append(td);

    element.append(tr);
}

var history_table_parent = null;

function fill_table_history(country) {
    if (history_table_parent === null) {
        history_table_parent = document.querySelector('#covid19-table').parentNode;
        // console.log('parent node stored')
    }

    document.querySelectorAll('#covid19-table_wrapper').forEach(function (item) {
        item.remove()
        // console.log('covid-19 wrapper removed')
    })
    document.querySelector('#history-tab').querySelectorAll('#covid19-table').forEach(function (itemm) {
        itemm.remove()
        // console.log('covid-19 wrapper removed')
    })

    var t = document.createElement('table')
    history_table_parent.appendChild(t)
    t.outerHTML = `
        <table class="created table table-bordered table-hover table-striped w-100" id="covid19-table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Country</th>
                <th scope="col">Date</th>
                <th scope="col">Confirmed</th>
                <th scope="col">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                         fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round"
                         stroke-linejoin="round"
                         class="feather feather-trending-up">
                        <polyline points="23 6 13.5 15.5 8.5 10.5 1 18"></polyline>
                        <polyline points="17 6 23 6 23 12"></polyline>
                    </svg>
                </th>
                <th scope="col">Active</th>
                <th scope="col">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                         fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round"
                         stroke-linejoin="round"
                         class="feather feather-trending-up">
                        <polyline points="23 6 13.5 15.5 8.5 10.5 1 18"></polyline>
                        <polyline points="17 6 23 6 23 12"></polyline>
                    </svg>
                </th>
                <th scope="col">Deaths</th>
                <th scope="col">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                         fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round"
                         stroke-linejoin="round"
                         class="feather feather-trending-up" style="color: rgb(214, 102, 121)">
                        <polyline points="23 6 13.5 15.5 8.5 10.5 1 18"></polyline>
                        <polyline points="17 6 23 6 23 12"></polyline>
                    </svg>
                </th>
                <th scope="col">Recovered</th>
                <th scope="col">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                         fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round"
                         stroke-linejoin="round"
                         class="feather feather-trending-up" style="color: rgb(113, 255, 47)">
                        <polyline points="23 6 13.5 15.5 8.5 10.5 1 18"></polyline>
                        <polyline points="17 6 23 6 23 12"></polyline>
                    </svg>
                </th>
            </tr>
            </thead>
            <tbody id="history-table-body">
            <tr id="history-loading-image">
                <td colspan="12">
                    <div class="d-flex justify-content-center">
                        <div class="spinner-border text-warning" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    `

    const element = document.querySelector('#history-table-body');
    element.innerHTML = loading_row;

    setTimeout(function () {
        let url = "https://api.covid19api.com/country/" + country;
        // console.log('url:' + url);
        const settings = {
            url: url,
            async: false,
            type: "GET",
            crossDomain: true,
            crossOrigin: true,
            success: function (result) {
                // console.log(result)
                result.reverse()
                show_data_in_element(element, result)
            },
            error: function (error) {
                show_error_in_element(element, error)
            },
            complete: function () {
                // $('#history-loading-image').hide();
            },
        };
        $.ajax(settings);
        var table = $('#covid19-table').DataTable();
        var table_wrapper = document.getElementById('covid19-table_wrapper')
        table_wrapper.classList.add('w-100')
        const els = table_wrapper.children;
        for (let e in els) {
            e = els[e]
            // console.log(e.classList)
            if (e.classList)
                e.classList.add('m-0')
        }
        table_wrapper.style.overflowX = 'scroll'
    }, 2000)
}

function get_country_data(country) {
    var res = [];
    $.ajax({
        url: "https://api.covid19api.com/country/" + country,
        async: false,
        type: "GET",
        crossDomain: true,
        crossOrigin: true,
        success: function (result) {
            result.reverse()
            res = result
        },
        error: function (error) {
        },
    });
    return res;
}

function fill_today_table() {
    const today_loading = $('#today-loading-image');
    today_loading.show();
    let countries = get_today_stats_of_all_countries()
    let table_body = document.getElementById('today-table-body')
    table_body.innerHTML = ''
    for (let i in countries) {
        let idx = i
        let val = countries[i]

        let tr = document.createElement('tr')

        const th = document.createElement('th');
        th.scope = 'row';
        th.innerText = (parseInt(idx) + 1).toString();
        tr.append(th);

        const Country = document.createElement('td');
        Country.innerText = val.location
        tr.append(Country);

        const Date = document.createElement('td');
        if (!('updated' in val))
            Date.innerText = '---';
        else
            Date.innerText = val.updated.split('.')[0];
        tr.append(Date);

        const confirmed = document.createElement('td');
        confirmed.innerText = numberWithCommas(val.confirmed)
        tr.append(confirmed);

        const active = document.createElement('td');
        active.innerText = numberWithCommas(parseInt(val.confirmed) - parseInt(val.dead) - parseInt(val.recovered));
        tr.append(active);

        const death = document.createElement('td');
        death.innerText = numberWithCommas(val.dead)
        tr.append(death);

        const recovered = document.createElement('td');
        recovered.innerText = numberWithCommas(val.recovered)
        tr.append(recovered);

        table_body.append(tr)
    }
    today_loading.hide();
    $('#today-table').DataTable();
    document.getElementById('today-table_wrapper').classList.add('w-100')
    const els = document.getElementById('today-table_wrapper').children;
    for (let e in els) {
        e = els[e]
        if (e.classList)
            e.classList.add('m-0')
    }
    document.getElementById('today-table_wrapper').style.overflowX = 'scroll'
}

function fill_news() {
    let news = get_news()
    var carousel_inner = document.getElementById('news-carousel-inner')
    var carousel_indicator = document.getElementById('news-carousel-top-indicator')
    let n = 4;

    let c = 0, i = 0;
    for (; c < n && i < news.length; i++) {
        let item = news[i]
        if (item.author === null || item.urlToImage === null) {
            i++;
            continue;
        }
        c++;
        // console.log(item)
        var indicator = document.createElement('li');
        carousel_indicator.append(indicator)
        indicator.outerHTML = '<li data-target="#featured" data-slide-to="' + (c - 1) + '" class="' + (c === 1 ? 'active' : '') + '"></li>'

        var carousel_item = document.createElement('div')
        carousel_item.classList.add('carousel-item')
        carousel_item.classList.add('h-100')
        carousel_item.classList.add('w-100')
        if (c === 1)
            carousel_item.classList.add('active')
        carousel_item.innerHTML = `<div class="card border-0 rounded-0 text-light overflow zoom h-100 w-100">
                                      <div class="position-relative h-100 w-100">
                                           <div class="ratio_left-cover-1 image-wrapper h-100 w-100">
                                              <a class="h-100 w-100" href="${item.url}">
                                                  <img style=" width: 100%; height: 100%; object-fit: contain;"
                                                      class="img-fluid h-100 w-100" src="${item.urlToImage}"
                                                      alt="${item.title}">
                                            </a>
                                          </div>
                                          <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">
                                               <a href="${item.url}">
                                                   <h2 class="h3 post-title text-white my-1">${item.title}</h2>
                                                </a>
                                              <div class="news-meta">
                                                   <span class="news-author">${item.author}</span>
                                                   <span class="news-date"> ${item.publishedAt.replace('T', ' ').replace('Z', '')}</span>
                                               </div>
                                           </div>
                                       </div>
                                  </div>`
        carousel_inner.append(carousel_item)
    }

    var sub_news_body = document.getElementById('sub_news_body')
    var classes = [
        'col-6 pb-1 pt-0 pr-1',
        'col-6 pb-1 pl-1 pt-0',
        'col-6 pb-1 pr-1 pt-1',
        'col-6 pb-1 pl-1 pt-1',
    ]

    for (; c < n + 4 && i < news.length; i++) {
        let item = news[i]
        if (item.author === null || item.urlToImage === null) {
            i++;
            continue;
        }
        c++;
        // console.log(item)

        var div = document.createElement('div')
        sub_news_body.append(div)
        div.outerHTML = '<div class="' + classes[c - n - 1] + '">\n' +
            '                    <div class="card border-0 rounded-0 text-white overflow zoom h-100 w-100">\n' +
            '                        <div class="position-relative h-100 w-100">\n' +
            '                            <div class="ratio_right-cover-2 image-wrapper h-100 w-100">\n' +
            '                                <a href="' + item.url + '" class=" h-100 w-100">\n' +
            '                                    <img class="img-fluid h-100 w-100" src="' + item.urlToImage + '" alt="' + item.title + '">\n' +
            '                                </a>\n' +
            '                            </div>\n' +
            '                            <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">\n' +
            '                                <a class="p-1 badge badge-primary rounded-0" href="' + item.url + '">Covid-19</a>\n' +
            '                                <a href="' + item.url + '">\n' +
            '                                    <h2 class="h5 text-white my-1">' + item.title + '</h2>\n' +
            '                                </a>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>'

    }
}

$(document).ready(function () {
    $(document).on('change', '#country', perform_query)
})